# base image
FROM node:10-alpine

# create application directory
RUN mkdir /app

# define process working directory
WORKDIR /app

# copy package files
COPY package.json package-lock.json ./

# install runtime dependencies
RUN npm install --production

# copy source code
COPY . .

# install ffmpeg
RUN apk add --no-cache ffmpeg

# declare environment variables
ENV PORT=80

# expose port
EXPOSE $PORT

# start command
CMD npm run prod:server