# pi-video-streaming

A demo for a centralized server, that publishes video streams from Raspberry Pis to a web application. Heavily inspired by: [https://github.com/jaredpetersen/raspi-live](https://github.com/jaredpetersen/raspi-live)

## Usage

To start the central server, run:

```shell
$ npm run dev:server
```

To start the Raspberry Pi client, run:

```shell
$ npm run dev:pi
```
