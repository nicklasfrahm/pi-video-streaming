const socket = io();

const App = () => {
  const [route, setRoute] = React.useState(window.location.hash);
  const navigate = () => {
    setRoute(window.location.hash);
  };
  React.useEffect(() => {
    window.addEventListener('hashchange', navigate);
    if (route === '') {
      window.location.hash = '/';
    }
    return () => {
      window.removeEventListener('hashchange', navigate);
    };
  }, []);
  if (route === '#/') return <DeviceList socket={socket} route={route} />;
  if (/^#\/streams/.test(route)) return <DeviceStream route={route} />;
  return null;
};

ReactDOM.render(<App />, document.getElementById('app'));
