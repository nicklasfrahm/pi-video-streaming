window.DeviceList = ({ socket }) => {
  const [state, setState] = React.useState({
    devices: []
  });
  React.useEffect(() => {
    fetch('/api/devices')
      .then(response => response.json())
      .then(json => {
        setState({
          ...state,
          devices: json.data
        });
        socket.on('devices.registered', device => {
          setState({
            ...state,
            devices: [...state.devices, device]
          });
        });
        socket.on('devices.unregistered', device => {
          setState({
            ...state,
            devices: state.devices.filter(item => item.id !== device.id)
          });
        });
      });
  }, []);
  return (
    <div>
      <div>
        {state.devices.length ? (
          <div>
            <p>
              <b>
                {state.devices.length}{' '}
                {state.devices.length > 1 ? 'devices are' : 'device is'}{' '}
                connected.
              </b>
            </p>
            <ul>
              {state.devices.map(device => (
                <li key={device.id}>
                  <span>{device.timestamp}</span>
                  <br />
                  <a href={`/watch/${device.id}`}>{device.id}</a>
                </li>
              ))}
            </ul>
          </div>
        ) : (
          <p>
            <b>There are currently no devices connected.</b>
          </p>
        )}
      </div>
    </div>
  );
};
