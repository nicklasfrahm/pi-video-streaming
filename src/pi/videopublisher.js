const { Writable } = require('stream');

class VideoPublisher extends Writable {
  constructor(options) {
    const { socket, ...config } = options;
    super(config);
    this.socket = socket;
  }
  _write(chunk, encoding, callback) {
    this.socket.binary(true).emit('video.data', chunk, encoding, callback);
  }
}

module.exports = socket => new VideoPublisher({ socket });
