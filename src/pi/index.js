const { spawn } = require('child_process');
const socketIoClient = require('socket.io-client');
const createVideoPublisher = require('./videopublisher');
const config = require('../../config.json');

const apiUrl = process.env.API_URL || 'http://localhost:8080';
const socket = socketIoClient(apiUrl);
let raspividProcess = null;

socket.on('disconnect', () => {
  console.log(`Disconnected from server: ${apiUrl}`);
  if (raspividProcess) {
    raspividProcess.kill();
  }
});

socket.on('connect', () => {
  console.log(`Connected to server: ${apiUrl}`);
  const nodeName = 'node-abcdef0123456789';

  socket.emit('devices.registration', nodeName, ({ id, timestamp }) => {
    console.log(`Registered as: ${id}`);
    console.log(`Registered at: ${timestamp}`);

    // configure raspivid commandline utility options
    const raspividOptions = [];
    // direct stream to stdout
    raspividOptions.push('-o', '-');
    // enable real-time streaming
    raspividOptions.push('-t', '0');
    // configure video size
    raspividOptions.push('-w', config.width, '-h', config.height);
    // set up framerate
    raspividOptions.push('-fps', config.framerate);
    // specifies the number of frames between each I-frame
    raspividOptions.push('-g', config.framerate);
    // configure quality profile
    raspividOptions.push('-pf', config.profile);
    // set up bitrate
    raspividOptions.push('-b', config.bitrate);
    // flip / mirror video
    if (config.horizontalFlip) raspividOptions.push('-hf');
    if (config.verticalFlip) raspividOptions.push('-vf');

    raspividProcess = spawn('raspivid', raspividOptions);
    raspividProcess.stdout.pipe(createVideoPublisher(socket));

    console.log(`Running raspivid with PID: ${raspividProcess.pid}`);

    process.on('uncaughtException', () => {
      raspividProcess.kill();
    });
  });
});
