const uuid = require('uuid/v4');

// object to store connected devices
const devices = {};
// lookup-table to resolve socket ids to public ids and vice-versa
const sidToId = {};
const idToSid = {};

exports.create = (socket, data = {}, id = uuid()) => {
  while (!id || devices[id]) {
    id = uuid();
  }
  const timestamp = new Date().toISOString();
  sidToId[socket.id] = id;
  idToSid[id] = socket.id;
  devices[id] = {
    id,
    timestamp,
    socket,
    data
  };
  return devices[id];
};

exports.get = (idOrSid, isSocketId = false) => {
  if (isSocketId) {
    return devices[sidToId[idOrSid]] || null;
  }
  return devices[idOrSid] || null;
};

exports.find = () => {
  return Object.keys(devices).map(id => devices[id]);
};

exports.delete = (idOrSid, isSocketId) => {
  let id = '';
  let sid = '';
  if (isSocketId) {
    id = sidToId[idOrSid];
    sid = idOrSid;
  } else {
    id = idOrSid;
    sid = idToSid[idOrSid];
  }
  const device = devices[id];
  delete sidToId[sid];
  delete idToSid[id];
  delete devices[id];
  return device || null;
};
