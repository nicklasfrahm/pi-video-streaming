const express = require('express');
const cors = require('cors');
const compression = require('compression');
const os = require('os');
const http = require('http');
const path = require('path');
const fs = require('fs-extra');
const hbs = require('express-handlebars');
const socketio = require('socket.io');
const Device = require('./device');
const io = require('./io');

const app = express();
const server = http.Server(app);
const sio = socketio(server);
const port = process.env.PORT || 8080;
const clientDir = path.resolve(__dirname, '../client');
const videoDir = path.resolve(os.tmpdir(), 'pi-video-streaming');

if (fs.existsSync(videoDir)) {
  fs.removeSync(videoDir);
}
fs.mkdirSync(videoDir);

app.use(cors());
app.use(compression({ level: 9 }));
app.use('/static', express.static(path.resolve(clientDir, 'static')));
app.use('/streams', express.static(videoDir));

app.engine(
  'hbs',
  hbs({
    extname: '.hbs',
    defaultLayout: 'default',
    layoutDir: path.resolve(clientDir, 'layouts')
  })
);
app.set('view engine', 'hbs');
app.set('views', path.resolve(clientDir, 'views'));

app.get('/', (req, res) => {
  const indexFile = path.join(clientDir, 'index.html');
  res.status(200);
  fs.createReadStream(indexFile).pipe(res);
  return;
});

app.get('/watch/:device', (req, res) => {
  return res.status(200).render('stream', { device: req.params.device });
});

app.get('/api/devices', (req, res) => {
  return res.status(200).json({
    data: Device.find().map(({ id, timestamp, data }) => ({
      id,
      timestamp,
      data
    }))
  });
});

sio.on('connection', io.streaming(videoDir));
sio.on('connection', io.registration());

server.listen(port, () => {
  console.log(`[API] Server online: 0.0.0.0:${port}`);
});
