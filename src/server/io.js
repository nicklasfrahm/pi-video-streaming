const path = require('path');
const fs = require('fs-extra');
const glob = require('glob');
const ffmpeg = require('fluent-ffmpeg');
const { EventEmitter } = require('events');
const Device = require('./device');
const createVideoSubscriber = require('./videosubscriber');
const config = require('../../config');

const devicePubSub = new EventEmitter();

exports.registration = () => socket => {
  socket.once('devices.registration', (id, reply) => {
    const device = Device.create(socket, {}, id);
    const { timestamp, data } = device;
    console.log(`[SIO] Device registered: ${id}`);
    socket.broadcast.emit('devices.registered', { id, timestamp, data });
    devicePubSub.emit('devices.registered', { id, timestamp, data });
    reply({ id, timestamp, data });
  });

  socket.once('disconnect', () => {
    const device = Device.delete(socket.id, true);
    if (device) {
      const { id, timestamp, data } = device;
      console.log(`[SIO] Device unregistered: ${id}`);
      socket.broadcast.emit('devices.unregistered', { id, timestamp, data });
      devicePubSub.emit('devices.unregistered', { id, timestamp, data });
    }
  });
};

exports.streaming = directory => socket => {
  let conversionStream = null;
  let videoStream = null;

  const cleanup = (device, err) => {
    if (err) console.error(err);
    if (conversionStream) conversionStream.destroy();
    if (videoStream) videoStream.destroy();
    glob(path.join(directory, `${device.id}*`), async (err, files) => {
      if (files) {
        await Promise.all(files.map(file => fs.remove(file)));
        console.log(`[VID] Purged stale video files from: ${device.id}`);
      }
    });
  };

  devicePubSub.once('devices.unregistered', device => cleanup(device));

  devicePubSub.once('devices.registered', device => {
    const dev = Device.get(socket.id, true);
    if (dev && dev.id === device.id) {
      videoStream = createVideoSubscriber(socket);

      const outputOptions = [
        '-seg_duration',
        config.time,
        '-window_size',
        config.listSize,
        '-extra_window_size',
        config.storageSize,
        '-init_seg_name',
        `${device.id}-init.m4s`,
        '-media_seg_name',
        `${device.id}-$Time$-$Number$.m4s`
      ];

      conversionStream = ffmpeg(videoStream)
        .inputOptions(['-re'])
        .noAudio()
        .videoCodec('copy')
        .format('dash')
        .outputOptions(outputOptions)
        .output(path.join(directory, `${device.id}-manifest.mpd`))
        .on('start', () => console.log(`[VID] Streaming from: ${device.id}`))
        .on('error', err => cleanup(device, err))
        .run();
    }
  });
};
