const { Readable } = require('stream');

class VideoSubscriber extends Readable {
  constructor(options) {
    const { socket, ...config } = options;
    super(config);
    this.socket = socket;
    this.socket.on('video.data', (chunk, encoding, callback) => {
      this.push(chunk);
      callback();
    });
  }
  _read() {
    this.push(Buffer.from([]));
  }
}

module.exports = socket => new VideoSubscriber({ socket });
